interface SongDetails {
  id: number;
  title: string;
  imageUrl: string;
  songUrl: string;
}

export default SongDetails;
