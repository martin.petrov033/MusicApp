interface Artists {
  id: number;
  name: string;
  url: string;
}

export default Artists;
